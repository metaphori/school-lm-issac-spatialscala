package it.unibo.spala.programs

import akka.actor.ActorRef
import it.unibo.spala.actors.system.SpatialSystem
import it.unibo.spala.config.{Settings, SystemSettings}
import it.unibo.spala.domain.DeviceBehaviors
import it.unibo.spala.math.Point3D
import it.unibo.spala.InputData

import scala.concurrent.duration._

/**
 * Created by: Roberto Casadei 
 * Created on date: 20/07/15.
 */

object CmdLineProgram extends App {

  type State = Int
  type Input = Boolean

  val parser = it.unibo.spala.config.CmdLineParser[State]

  parser.parse(args, Settings[State](
    systemSettings = SystemSettings(start = false))
  ) match {
    case Some(settings) =>
      var stgs: Settings[State] = settings.copy(deviceSettings = settings.deviceSettings.copy(
        initialDeviceState = 0,
        inputProvider = (k:Int, name:String, ref:ActorRef, pos:Point3D) => List(InputData("isSrc", k>0 && k%50==0)),
        nameProvider = (k:Int) => "d"+k,
        workInterval = 500 millis
      ))
      new SpatialSystem[State](stgs, () => new DeviceBehaviors.GradientBehavior)
    case None => parser.showUsage
  }

}
