package it.unibo

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

/**
 * Created by: Roberto Casadei 
 * Created on date: 08/07/15.
 */
package object spala {
  val Log = Logger(LoggerFactory.getLogger("SpalaLogger"))
}
