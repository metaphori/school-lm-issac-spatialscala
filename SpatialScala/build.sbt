name := "SpatialScala"

organization := "it.unibo"

version := "1.0"
 
scalaVersion := "2.11.6"
 
resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

publishMavenStyle := true

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor"  % "2.3.4",
  "com.typesafe.akka" %% "akka-remote" % "2.4-SNAPSHOT",
  "com.typesafe.akka" %% "akka-cluster" % "2.4-SNAPSHOT",
  "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
  "org.slf4j" % "slf4j-log4j12" % "1.7.12",
  "com.github.scopt" %% "scopt" % "3.3.0" // for parsing cmd-line options
)
